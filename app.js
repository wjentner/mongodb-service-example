var MongoClient = require('mongodb').MongoClient,
    assert = require('assert');

// Open the connection to the server
MongoClient.connect("mongodb://test123:test123@mongo:27017/test", function(err, db) {
    assert.equal(null, err);
    assert.ok(db !== null);

    db.collection("testcollection").update({a:1}, {b:1}, {upsert:true}, function(err, result) {
        assert.equal(null, err);
        assert.ok(result);
        console.log('Result: '+result);
        assert.equal(1, result.result.n);
        db.close();
    });
});

//test failed authentication
MongoClient.connect("mongodb://test123:WRONGPW@mongo:27017/test", function(err) {
    assert.notEqual(null, err);
    console.log('Error Message: '+err.message);
    assert.equal('Authentication failed.', err.message);
});